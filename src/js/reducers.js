import {combineReducers} from 'redux';
import multireducer from 'multireducer';

import LayoutReducer from './containers/Layout/reducer';
import NavigationReducer from './containers/Navigation/reducer';
import TodoListReducer from './containers/TodoList/reducer';


const reducers = combineReducers({
    LayoutReducer: LayoutReducer,
    NavigationReducer: NavigationReducer,
    TodoListCollection: multireducer({
        todoListA: TodoListReducer,
        todoListB: TodoListReducer,
    }),
});

export default reducers;