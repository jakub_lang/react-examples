import React, {Component} from 'react';

class Dashboard extends Component {
    constructor () {
        super();
        console.log('constructor page: Dashboard');
    }

    render() {
        console.log('rendering page: Dashboard');

        return (
        <div id="content">
            <div className="row">
                <div className="col-lg-12">
                    <h1 className="page-header">Dashboard</h1>
                </div>
            </div>
            <div className="row">
                Welcome to my first react app.
            </div>
        </div>
        );
    }
}

export default Dashboard;