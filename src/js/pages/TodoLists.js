import React, { Component } from 'react';

import TodoList from '../containers/TodoList/index';

class TodoLists extends Component {
    constructor () {
        super();
        console.log('constructor page: TodoLists');
    }

    render () {
        console.log('render page: TodoLists');

        return (
            <div id="content">
                <div className="row">
                    <div className="col-lg-12">
                        <h1 className="page-header">My todos</h1>
                    </div>
                </div>
                <div className="row">

                    <div className="col-sm-12">
                        <TodoList title="Prvni todolist" as="todoListA"/>
                        <TodoList title="Druhy todolist" as="todoListB"/>
                    </div>
                </div>
            </div>
        );
    }
}

export default TodoLists;