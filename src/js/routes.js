import React from 'react';
// import { Route, IndexRoute } from 'react-router-dom';
import { Route, IndexRoute } from 'react-router';

import App from './App';
import pages from './pages';

const routes = () => (
    <Route path="/" component={App}>
        <IndexRoute component={pages.Dashboard} />
    </Route>
);

export default routes;