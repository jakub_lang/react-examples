import * as Constants from './constants';

export const addItem = (item) => {
    return {
        type: Constants.TODOLIST_ACTION_ADD_ITEM,
        item: item
    };
};

export const editItem = (item) => {
    return {
        type: Constants.TODOLIST_ACTION_EDIT_ITEM,
        item: item
    };
};

export const updateItem = (item) => {
    return {
        type: Constants.TODOLIST_ACTION_UPDATE_ITEM,
        item: item
    };
};

export const removeItem = (item) => {
    return {
        type: Constants.TODOLIST_ACTION_REMOVE_ITEM,
        item: item
    };
};