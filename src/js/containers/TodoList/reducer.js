import {fromJS} from 'immutable';
import * as Constants from './constants';
import * as Helpers from '../../helpers';

export const initialState = fromJS({
    items: [],
    nextId: 1,
});

const TodoListReducer = (state = initialState, action) => {
    switch (action.type) {
        case Constants.TODOLIST_ACTION_ADD_ITEM:
            let newId = state.get('nextId');
            let item = action.item;

            item.id = newId;

            let newItems = state.get('items').push(item);

            return state.set('items', newItems).set('nextId', newId + 1);

        case Constants.TODOLIST_ACTION_EDIT_ITEM:
            index = Helpers.getIndexInListById(state, state.get('items'), action.item.id);
            return state.updateIn(['items', index], index => Helpers.getCopyOfObject(action.item));

        case Constants.TODOLIST_ACTION_UPDATE_ITEM:
            let index = Helpers.getIndexInListById(state, state.get('items'), action.item.id);
            return state.updateIn(['items', index], index => Helpers.getCopyOfObject(action.item));

        case Constants.TODOLIST_ACTION_REMOVE_ITEM:
            return state.update('items', items => items.splice(Helpers.getIndexInListById(state, state.get('items'), action.item.id), 1));

        default:
            return state;
    }
};

export default TodoListReducer;