import React, {Component} from 'react';
import {bindActionCreators} from 'multireducer';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

import * as Actions from './actions';

class TodoListContainer extends Component {
    constructor() {
        super();

        this.renderItem = this.renderItem.bind(this);
        this.addItem = this.addItem.bind(this);
    }

    removeItem(item) {
        this.props.actions.removeItem(item);
    }

    addItem() {
        let item = {
            value: '',
            editing: true
        };

        this.props.actions.addItem(item);
    }

    editItem(item) {
        item.editing = true;

        this.props.actions.editItem(item);
    }

    updateItem(item) {
        item.value = this.refs[`newItemInput-${item.id}`].value;
        item.editing = false;

        this.props.actions.updateItem(item);
    }

    renderItem(item) {
        if (item.editing) {
            return this.renderItemEditing(item);
        } else {
            return this.renderItemDefault(item);
        }
    }

    renderItemDefault(item) {
        return (
            <tr key={item.id}>
                <td>{item.value}</td>

                <td className="table-action">
                    <button className="btn btn-xs btn-primary" onClick={this.editItem.bind(this, item)}>
                        <i className="fa fa-pencil" />
                    </button>
                </td>

                <td className="table-action">
                    <button className="btn btn-xs btn-danger" onClick={this.removeItem.bind(this, item)}>
                        <i className="fa fa-times" />
                    </button>
                </td>
            </tr>
        );
    }

    renderItemEditing(item) {
        return (
            <tr key={item.id}>
                <td colSpan="2">
                    <div className="form-group">
                        <input type="text" className="form-control" ref={`newItemInput-${item.id}`} defaultValue={item.value} placeholder="Add some text" />
                    </div>
                </td>
                <td className="table-action">
                    <button className="btn btn-xs btn-success" onClick={this.updateItem.bind(this, item)}>
                        <i className="fa fa-check" />
                    </button>
                </td>
            </tr>
        );
    }

    render() {
        let content = (
            <div className="panel-body">
                Empty list
            </div>
        );

        let items = this.props.TodoListReducer.get('items');

        console.log('rendering component: TodoList');

        if (items.size > 0) {
            content = (
                <table className="table">
                    <tbody>
                        {items.map(this.renderItem)}
                    </tbody>
                </table>
            );
        }

        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    {this.props.title}
                </div>

                {content}

                <div className="panel-footer">
                    <button className="btn btn-primary" onClick={this.addItem}>Add new</button>
                </div>
            </div>
        );
    }
}

TodoListContainer.propTypes = {
    TodoListReducer: ImmutablePropTypes.map,
    actions: PropTypes.shape({
        addItem: PropTypes.func,
        editItem: PropTypes.func,
        updateItem: PropTypes.func,
        removeItem: PropTypes.func,
    }),
};

const mapStateToProps = (state, {as}) => ({
    TodoListReducer: state.TodoListCollection[as]
});

const mapDispatchToProps = (dispatch, {as}) => ({
    actions: bindActionCreators(Actions, dispatch, as)
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoListContainer);