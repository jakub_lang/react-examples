import {fromJS} from 'immutable';
import * as Constants from './constants';
import * as Helpers from '../../helpers';

export const initialState = fromJS({
    title: 'First examples',
});

const LayoutReducer = (state = initialState, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

export default LayoutReducer;