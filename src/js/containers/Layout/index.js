import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import * as BS from 'react-bootstrap';

import Navigation from '../Navigation/index';

import * as Actions from './actions';
import pages from '../../pages';

class LayoutContainer extends Component {

    render () {
        console.log('rendering Layout');

        return (
            <Router>
                <div className="container">

                    <header className="header">
                        <BS.Navbar fluid role="navigation">
                            <BS.Navbar.Header>
                                <BS.Navbar.Brand>
                                    <Link to="/">{this.props.LayoutReducer.get('title')}</Link>
                                </BS.Navbar.Brand>
                                <BS.Navbar.Toggle />
                            </BS.Navbar.Header>

                            <Navigation/>
                            
                        </BS.Navbar>
                    </header>

                    <div className="content">
                        <Switch>
                            <Route exact path="/" component={pages.Dashboard}/>
                            <Route path="/todo-lists" component={pages.TodoLists}/>
                        </Switch>
                    </div>

                </div>
            </Router>
        );
    }
}

LayoutContainer.propTypes = {
    LayoutReducer: ImmutablePropTypes.map,
    actions: PropTypes.shape({
        // selectItem: PropTypes.func,
    }),
};

const mapStateToProps = ( state ) => ({
    LayoutReducer: state.LayoutReducer
});

const mapDispatchToProps = ( dispatch ) => ({
    actions: bindActionCreators(Actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(LayoutContainer);