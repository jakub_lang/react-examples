import {fromJS} from 'immutable';
import * as Constants from './constants';
import * as Helpers from '../../helpers';

export const initialState = fromJS({
    items: [
        {title: 'Dashboard', link: '/', icon: 'fa fa-dashboard fa-fw'},
        {title: 'TodoLists', link: '/todo-lists'},
        {title: 'Counter', link: '/counter'},
        {title: 'Dropdown', items: [
            {title: 'Action 1'},
            {title: 'Action 2'},
            {title: 'Action 3'},
        ]},
    ],
});

const NavigationReducer = (state = initialState, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

export default NavigationReducer;