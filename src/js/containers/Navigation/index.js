import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { LinkContainer } from 'react-router-bootstrap';
import * as BS from 'react-bootstrap';

import * as Actions from './actions';

class NavigationContainer extends Component {
    constructor () {
        super();

        this.renderMenuItems = this.renderMenuItems.bind(this);
    }

    renderMenuItems ( item, i ) {
        let items = item.get('items');

        if ( items ) {
            return this.renderDropdown(item, i + 1);
        } else {
            return this.renderSingleItem(item, i + 1);
        }
    }

    renderSingleItem (item, key, type = 'nav-item') {
        let link = item.get('link');
        let icon = item.get('icon');
        let title = item.get('title');
        let content;

        if ( !link ) {
            link = '#';
        }

        if (icon) {
            icon = (<i className={icon} />);
        }

        switch ( type ) {
            case 'nav-item':
                content = (<BS.NavItem activeKey={key}>{icon} {title}</BS.NavItem>);
                break;
            case 'menu-item':
                content = (<BS.NavItem activeKey={key}>{icon} {title}</BS.NavItem>);
                break;
            default:
                content = (<a href={link}>{icon} {title}</a>);
        }

        return (
            <LinkContainer to={link} key={key}>
                {content}
            </LinkContainer>
        );
    }

    renderDropdown (item, key) {
        let title = item.get('title');
        let items = item.get('items');

        return (
            <BS.NavDropdown title={title} key={key} eventKey={key} id="basic-nav-dropdown">
                {items.map((item, i) => {
                    let newKey = `${key}.${i + 1}`;
                    return this.renderSingleItem(item, newKey);
                })}
            </BS.NavDropdown>
        );
    }

    render () {
        let items = this.props.NavigationReducer.get('items');

        return (
            <BS.Navbar.Collapse>
                <BS.Nav>
                    {items.map(this.renderMenuItems)}
                </BS.Nav>

                <BS.Nav pullRight>
                    <BS.NavItem href="#">Link Right</BS.NavItem>
                    <BS.NavItem href="#">Link Right</BS.NavItem>
                </BS.Nav>
            </BS.Navbar.Collapse>
        );
    }
}

NavigationContainer.propTypes = {
    NavigationReducer: ImmutablePropTypes.map,
    actions: PropTypes.shape({
        selectItem: PropTypes.func,
    }),
};

const mapStateToProps = ( state ) => ({
    NavigationReducer: state.NavigationReducer
});

const mapDispatchToProps = ( dispatch ) => ({
    actions: bindActionCreators(Actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(NavigationContainer);