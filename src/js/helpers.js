export const getCopyOfObject = (obj) => {
    return JSON.parse(JSON.stringify(obj));
};

export const getIndexInListById = (state, items, id) => {
    const index = items.findIndex(item => {
        return item.id === id;
    });

    return index;
};