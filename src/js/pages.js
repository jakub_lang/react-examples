import Dashboard from './pages/Dashboard';
import TodoLists from './pages/TodoLists';

const pages = {
    Dashboard: Dashboard,
    TodoLists: TodoLists,
};

export default pages;