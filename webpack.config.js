const path = require('path');

module.exports = {

    context: path.join(__dirname, 'src'),

    entry: [
        './js/index.js'
    ],

    devServer: {
        inline: true,
        contentBase: './public',
        port: 3000,
        historyApiFallback: true
    },

    output: {
        path: path.join(__dirname, 'public'),
        filename: 'js/bundle.js'
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    'babel-loader'
                ]
            }
        ]
    },

    resolve: {
        modules: [
            path.join(__dirname, 'node_modules')
        ]
    }
};