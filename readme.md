# React examples

## Run project for development

1. ```npm install```
2. ```npm run server``` (run local server)
3. visit **http://localhost:3000**

## Run project on server

1. ```npm run compile``` (compile all JS into one single file **bundle.js**)
2. include this file into footer of page